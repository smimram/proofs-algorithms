---
layout: default
title: Theoretical Cosynus Seminar
---

# The Theoretical Cosynus Seminar

The _theoretical cosynus seminar_ is aimed at PhD students and young researchers
(but older are also welcome) of the [Coynus
team](http://www.lix.polytechnique.fr/cosynus/) (but people from other teams are
also welcome) in order to have informal presentations about

- work in progress,
- research ideas,
- articles from other people,
- well-known works

in the areas of fundamental computer science. This is intended as a safe space
to discuss research being done, or share interest about research from
others. More mature works are typically presented in the [proofs and algorithms
seminar](https://www.lix.polytechnique.fr/proofs-algorithms/seminar/).

Everybody is welcome to subscribe to the
[mailing-list](https://sympa.lix.polytechnique.fr/lists/subscribe/tcs-seminar)
or to the [calendar](calendar.ics).

{% for talk in site.data.tcs %}
**{{talk.speaker}}** {% if talk.lab and talk.lab != "" %}({{talk.lab}}){% endif %} – _{{talk.title}}_<br/>
{{talk.date | date_to_long_string}} at {{talk.date | date: "%H:%M"}} in {{talk.room}}

{{talk.abstract}}
{% endfor %}
