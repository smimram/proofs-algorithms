---
layout: default
title: Team AlCo
---

# Presentation

The AlCo (Algorithms and Complexity) team is part of the [LIX
laboratory](http://www.lix.polytechnique.fr) at [École
Polytechnique](http://www.polytechnique.edu/). It is part of the Pole [_Proofs
and Algorithms_](https://www.lix.polytechnique.fr/proofs-algorithms/).

The AlCo team works on algorithms, and complexity and computability theory. It
works more concretely on analog computation models, constraint satisfaction
problems and their complexity in various computational models, probabilistic
analysis and approximation algorithms.

## Permanent Members

- Olivier Bournez (Polytechnique)
- Benjamin Doerr (Polytechnique)
- Miki Hermann (CNRS)
- Martin Krejca (Polytechnique)
- Hang Zhou (Poytechnique)

## Postdocs

- Riccardo Gozzi


## PhD Students

- Manon Blanc
- Valentin Dardilhac
- Johan Girardot

## Seminars

We have merged our seminar with the following two seminars:

- [The Proofs and algorithms pole seminar](https://www.lix.polytechnique.fr/proofs-algorithms/seminar/)
- [Séminare Algorithmique du Plateau de Saclay](https://bournez.gitlabpages.inria.fr/seminar-algorithms-plateau-Saclay/)
