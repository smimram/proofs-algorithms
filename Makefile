all:
	jekyll build

serve:
	jekyll serve

ci:
	git ci . -m "Updated website."
	git push

ssh:
	ssh ci@proofs-algorithms-ci.ci

ics: seminar/calendar.ics

seminar/calendar.ics: _data/seminar.yml _scripts/sem2ical.py
	cd _scripts && ./sem2ical.py
